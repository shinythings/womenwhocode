//
//  PETCollectionViewCell.h
//  PetR
//
//  Created by Sebastian Grail on 10/06/2014.
//  Copyright (c) 2014 Shiny Things. All rights reserved.
//

#import <UIKit/UIKit.h>

@class PETAnimal;

@interface PETCollectionViewCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UIImageView *imageView;

@end
