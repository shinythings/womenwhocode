//
//  PETBrowserDataSource.h
//  PetR
//
//  Created by Sebastian Grail on 18/06/2014.
//  Copyright (c) 2014 Shiny Things. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PETAnimal.h"

@interface PETBrowserDataSource : NSObject <UICollectionViewDataSource>

- (instancetype)initWithAnimalType:(PETAnimalType)type completionHandler:(void(^)())completionHandler;

@end
