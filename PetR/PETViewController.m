//
//  PETViewController.m
//  PetR
//
//  Created by Sebastian Grail on 12/06/2014.
//  Copyright (c) 2014 Shiny Things. All rights reserved.
//

#import "PETViewController.h"
#import "PETBrowserViewController.h"
#import "PETBrowserDataSource.h"

@interface PETViewController ()

@end

@implementation PETViewController

- (void)viewDidLoad {
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
	
	[[[NSOperationQueue alloc] init] addOperationWithBlock:^{
		NSURL *url = [NSURL URLWithString:@"http://shiny-pets.herokuapp.com/pets/count.json"];
		NSData *data = [NSData dataWithContentsOfURL:url];
		NSDictionary *dictionary = [NSJSONSerialization JSONObjectWithData:data options:0 error:NULL];
		[[NSOperationQueue mainQueue] addOperationWithBlock:^{
			self.numberOfPetsLookingForHomeLabel.text = [NSString stringWithFormat:@"%@ pets looking for a new home", dictionary[@"count"]];
		}];
	}];
	
	// TODO: Set a random cat/dog image
	/*
	 Add the buttons as properties to the view controller (control-drag from the button to the public interface at the top of the header file)
	 Get a dictionary of all dogs from http://shiny-pets.herokuapp.com/pets/dogs.json (/cats.json for cats)
	 The dictionary contains an array of pets for the key @"pets"
	 Use arc4random_inform(upperLimit) to get a random integer between 0 and upperLimit
	 Use that index to get a dictionary of a random pet from the array
	 The dictionary contains an image url string for the key @"image_url"
	 Create an NSURL object from that string
	 Create an NSData object with the contents of that URL
	 Create a UIImage object with the image data ([UIImage imageWithData:])
	 assign the image to the button's image property ([self.catButton setImage:image forState:0])
	 Use an NSOperationQueue to perform the web service requests in the background and another queue to set the image on the main queue.
	 */
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
	PETBrowserViewController *vc = segue.destinationViewController;
	vc.dataSource = [[PETBrowserDataSource alloc] initWithAnimalType:PETAnimalType_Cat completionHandler:^{
		[vc.collectionView reloadData];
	}];
	
	// FIXME: The dog button transitions to a collection of cats.
	/*
	 You can get the name of a segue from its identifier property
	 Compare the names of the segue to the names set in the storyboard ("Browse Cats" and "Browse Dogs")
	 	Use the isEqualToString: method to compare strings
	 use if/else to set the correct datasource on the destination view controller
	 */
}

@end
