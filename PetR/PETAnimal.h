//
//  PETAnimal.h
//  PetR
//
//  Created by Sebastian Grail on 11/06/2014.
//  Copyright (c) 2014 Shiny Things. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSInteger, PETAnimalType) {
	PETAnimalType_Cat,
	PETAnimalType_Dog
};

@interface PETAnimal : NSObject

@property (nonatomic, readonly) NSString *name;
@property (nonatomic, readonly) NSString *description;
@property (nonatomic, readonly) PETAnimalType type;
@property (nonatomic, readonly) UIImage *image;


- (instancetype)initWithDictionary:(NSDictionary*)dict;


@end
