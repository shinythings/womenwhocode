//
//  PETAppDelegate.h
//  PetR
//
//  Created by Sebastian Grail on 12/06/2014.
//  Copyright (c) 2014 Shiny Things. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PETAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
