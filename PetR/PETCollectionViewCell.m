//
//  PETCollectionViewCell.m
//  PetR
//
//  Created by Sebastian Grail on 10/06/2014.
//  Copyright (c) 2014 Shiny Things. All rights reserved.
//

#import "PETCollectionViewCell.h"
#import "PETAnimal.h"

@interface PETCollectionViewCell ()

@end


@implementation PETCollectionViewCell

- (void)prepareForReuse {
	[super prepareForReuse];
	
	self.imageView.image = nil;
}

@end
