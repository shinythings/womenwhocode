//
//  PETViewController.h
//  PetR
//
//  Created by Sebastian Grail on 12/06/2014.
//  Copyright (c) 2014 Shiny Things. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PETViewController : UIViewController

@property (weak, nonatomic) IBOutlet UILabel *numberOfPetsLookingForHomeLabel;

@end
