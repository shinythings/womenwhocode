//
//  PETBrowserViewController.h
//  PetR
//
//  Created by Sebastian Grail on 18/06/2014.
//  Copyright (c) 2014 Shiny Things. All rights reserved.
//

#import <UIKit/UIKit.h>

@class PETBrowserDataSource;

@interface PETBrowserViewController : UICollectionViewController

@property (nonatomic) PETBrowserDataSource *dataSource;

@end
