//
//  PETAnimal.m
//  PetR
//
//  Created by Sebastian Grail on 11/06/2014.
//  Copyright (c) 2014 Shiny Things. All rights reserved.
//

#import "PETAnimal.h"

@interface PETAnimal ()

@property (nonatomic, readonly) NSURL *imageURL;

@end


@implementation PETAnimal

- (instancetype)initWithDictionary:(NSDictionary*)dict {
	if (self = [super init]) {
		_description = dict[@"description"];
		_type = [dict[@"type"] integerValue];
		_imageURL = [NSURL URLWithString:dict[@"image_url"]];
		
	}
	return self;
}

@synthesize image = _image;
- (UIImage*)image {
	if (!_image) {
		NSData *data = [NSData dataWithContentsOfURL:self.imageURL];
		_image = [UIImage imageWithData:data];
	}
	return _image;
}

@end
