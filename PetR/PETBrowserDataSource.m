//
//  PETBrowserDataSource.m
//  PetR
//
//  Created by Sebastian Grail on 18/06/2014.
//  Copyright (c) 2014 Shiny Things. All rights reserved.
//

#import "PETBrowserDataSource.h"
#import "PETAnimal.h"
#import "PETCollectionViewCell.h"

@interface PETBrowserDataSource ()

@property (nonatomic, assign) PETAnimalType animalType;
@property (copy) NSArray *animals;

@end


@implementation PETBrowserDataSource

- (instancetype)initWithAnimalType:(PETAnimalType)type completionHandler:(void (^)())completionHandler {
	if (self=[super init]) {
		
		[[[NSOperationQueue alloc] init] addOperationWithBlock:^{
			NSURL *url;
			if (type == PETAnimalType_Cat) {
				url = [NSURL URLWithString:@"http://shiny-pets.herokuapp.com/pets/cats.json"];
			} else {
				url = [NSURL URLWithString:@"http://shiny-pets.herokuapp.com/pets/dogs.json"];
			}
			NSData *data = [NSData dataWithContentsOfURL:url];
			NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:data options:0 error:NULL];
			NSMutableArray *pets = [[NSMutableArray alloc] init];
			NSArray *allPets = dict[@"pets"];
			for (NSDictionary *petDictionary in allPets) {
				PETAnimal *pet = [[PETAnimal alloc] initWithDictionary:petDictionary];
				[pets addObject:pet];
			}
			
			self.animals = pets;
			if (completionHandler) {
				[[NSOperationQueue mainQueue] addOperationWithBlock:^{
					completionHandler();
				}];
			}
		}];
	}
	return self;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
	return self.animals.count;
}

- (UICollectionViewCell*)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
	PETCollectionViewCell *result = [collectionView dequeueReusableCellWithReuseIdentifier:@"CollectionViewCell" forIndexPath:indexPath];

	PETAnimal *pet = self.animals[indexPath.row];
	
	[[[NSOperationQueue alloc] init] addOperationWithBlock:^{
		UIImage *image = pet.image;
		[[NSOperationQueue mainQueue] addOperationWithBlock:^{
			PETCollectionViewCell *cell = (id)[collectionView cellForItemAtIndexPath:indexPath];
			if (cell) {
				cell.imageView.image = image;
			}
		}];
	}];
	
	return result;
}


@end
