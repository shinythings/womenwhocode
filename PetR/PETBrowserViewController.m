//
//  PETBrowserViewController.m
//  PetR
//
//  Created by Sebastian Grail on 18/06/2014.
//  Copyright (c) 2014 Shiny Things. All rights reserved.
//

#import "PETBrowserViewController.h"
#import "PETBrowserDataSource.h"

@interface PETBrowserViewController ()

@end

@implementation PETBrowserViewController

- (void)setDataSource:(PETBrowserDataSource *)dataSource {
	_dataSource = dataSource;
	self.collectionView.dataSource = dataSource;
}

@end
