//
//  main.m
//  PetR
//
//  Created by Sebastian Grail on 12/06/2014.
//  Copyright (c) 2014 Shiny Things. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "PETAppDelegate.h"

int main(int argc, char * argv[])
{
	@autoreleasepool {
	    return UIApplicationMain(argc, argv, nil, NSStringFromClass([PETAppDelegate class]));
	}
}
